/*
 * US1PolanskiBajorek.c
 *
 *  Created: 2015-11-04 16:15:47
 *  Authors: Artur Polański, Aleksandra Bajorek
 *  Cwiczenia I, gr. 1
 *  Zadanie dodatkowe
 */

#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>

int tab[] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
int sily[] = {0,0,0,0,0,0,0,0}; //tablica aktualnych sił wiecenia diody
int zmiany[] = {1,0,0,0,0,0,0,0}; //tablica aktualnych zmian sił, 1 -zwiększanie siły, -1 -zmniejszanie siły, 0 -brak zmian

void no () {
    for (int i = 0; i < 100; i++) {
        int swiatelka = 0x00; //diody, które maja swiecić
        for (int j = 0; j < 8; j++) {
            if (100 - sily[j] <= i)
                swiatelka += tab[j]; //każdą diodę świecimy tyle, ile wynosi jej siła
        }
        PORTA = swiatelka; //zaswiecenie
        _delay_us(10);
    }

    for (int i = 0; i < 8; i++) {
        sily[i] += zmiany[i];
        //jeśli świeci pełną siłą, zmień zmiany na -1 (rozpocznij zmiejszanie siły)
        if (sily[i] == 100)
            zmiany[i] = -1;
            //jeli nie świeci, zmień zmiany na 0 (zostajemy przy nieświeceniu)
        else if (sily[i] == 0)
            zmiany[i] = 0;
        //jeli dioda świeci z siłą 25 i jej siła wzrasta (oraz to nie ostatnia dioda) zmień zmiany kolejnej diody na 1 (rozpocznij zwiększanie siły kolejnej)
        if (sily[i] >= 25 && zmiany[i] > 0 && i != 7)
            zmiany[i + 1] = 1;
        //jeśli żadna dioda nie świeci, zmień zmiany pierwszej na 1 (rozpocznij zwiększanie siły tej diody)
        if (sily[0] == 0 && sily[1] == 0 && sily[2] == 0 && sily[3] == 0 && sily[4] == 0 &&
            sily[5] == 0 && sily[6] == 0 && sily[7] == 0)
            zmiany[0] = 1;
    }

}


int main(void)
{
    DDRA = 0xFF;
    PORTA = 0x01;
    while(1)
    {
        no();
    }
}