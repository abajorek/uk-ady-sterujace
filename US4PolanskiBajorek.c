/*
 * US4PolanskiBajorek.c
 *
 * Created: 2015-12-02 16:15:08
 *  Author: Aleksandra Bajorek, Artur Polański
 */
 
 #define F_CPU 16000000UL
 #include <avr/io.h>
 #include <util/delay.h>
 #include "avr/interrupt.h"
 #include <math.h>

// CYfry od 0 do 9 
 int cyfry[] = {~0b00111111, ~0b00000110, ~0b01011011, ~0b01001111, ~0b01100110, ~0b01101101, ~0b01111101, ~0b00000111, ~0b01111111, ~0b01101111};

 int licznik = 0;					//aktualnie wyświetlana liczba
 int pozycja=8;						//wyświetlacz na którym aktualnie wyświetlana jest cyfra
 int milisekundy = 0;				//licznik milisekund
 int wyswietlacz[] = {0,0,0,0};		
 int kolumna;						//kolumna w której został wciśnięty przycisk
 int wiersz;						//wiersz w którym został wciśniety przycisk
 
 int naiwny_log(int liczba)		 //funkcja zwracająca potrzebne wartości logarytmu
 {
     if(liczba==8) return 3;
     else if(liczba==4) return 2;
     else if(liczba==2) return 1;
     return 0;
 }
 
 void config_kolumn()			//konfiguracja do odczytu kolumny, w której wciśnięty jest przycisk
 {
     DDRC = 0xF0;
     PINC = 0x0F;
     PORTC = 0x0F;
 }
 
 int odczyt_kolumn()	
 {
     config_kolumn();			//konfiguracja
     for(int i=0; i<32; i++)	//czas na ustawienie konfiguracji
        ;
     return 15-PINC;			//odczyt kolumny
								//zwraca 8 gdy I kolumna, 4 gdy II kolumna, 2 gdy III kolumna, 1 gdy IV kolumna
 }
 
 void config_wierszy()			//konfiguracja do odczytu wiersza
 {
     DDRC = 0x0F;
     PINC = 0xF0;
     PORTC = 0xF0;
 }
 int odczyt_wierszy()
 {
     config_wierszy();			//konfiguracja
     for(int i=0; i<32; i++)	//czas na ustawienie konfiguracji
        ;
     return 15 - PINC/16;		//odczyt wiersza
								//zwraca 8 gdy I wiersz, 4 gdy II wiersz, 2 gdy III wiersz, 1 gdy IV wiersz
 }
 
 void wyswietl_licznik()		//funkcja wyświetlająca odpowiednią wartość na wyświetlaczach (z poprzednich ćwiczeń)
 {
     wyswietlacz[0]=licznik%10;
     wyswietlacz[1]=(licznik/10)%10;
     wyswietlacz[2]=(licznik/100)%10;
     wyswietlacz[3]=(licznik/1000)%10;
     PORTA = cyfry[wyswietlacz[milisekundy%4]];
     PORTB = ~pozycja;
     if(pozycja == 1)
     pozycja = 8;
     else
     pozycja= pozycja/2;
 }
 
 ISR(TIMER0_COMP_vect)
 {
         kolumna = odczyt_kolumn();
         wiersz = odczyt_wierszy();
         
         int liczbaWiersz=0;
         int liczbaKolumna=0;
         liczbaWiersz = 4*(3-naiwny_log(wiersz));					//obliczanie nr wiersza (numerując od 0 do 3) oraz mnożenie przez 4, ponieważ 
																	//wszystkie przyciski w I rzędzie mają wartość (1,4) w drugim (5,8) itd., mamy więc podstawę do obliczenia numeru przycisku
         liczbaKolumna = liczbaWiersz + 4-naiwny_log(kolumna);		//obliczanie nr kolumny (numerując od 1 do 4) oraz dodawanie do obliczonej wcześniej liczbaWiersza
																	//otrzymujemy numer wcieśniętego przycisku
		 if (wiersz*kolumna==0)										//jeśli nie jest wciśniety żaden przycisk, lub odczytana została tylko jedna wartość (np tylko wiesz lub tylko kolumna)
            licznik=0;												// licznik = 0
         else														//jeśli wciśnięty jest przycik i poprawnie odczytano obie wartości
            licznik=liczbaKolumna;									// licznik = numer wcisnietego przycisku
         wyswietl_licznik();										//wyświetlamy licznik
		 
         milisekundy++;
         if(milisekundy==1000)
            milisekundy=0;
 }
 
 void config()	//konfiguracja wyswielacza
 {
     OCR0 = 250;
     TCCR0 |= (0<<WGM00) | (1<< WGM01);
     TCCR0 |= (1<<CS00) | (1<< CS01) | (0<<CS02);
     TIMSK |= (1<<OCIE0);
     sei();
 }

 int main(void)
 {
     
     DDRA = 0xFF;
     PORTA = 0xFF;
     
     DDRB = 0xFF;
     PORTB = 0x0F;
     
     config();
     while(1)
     {
         
     }
 }