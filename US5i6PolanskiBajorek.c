/*
 * US5i6PolanskiBajorek.c
 *
 * Created: 2015-12-09 16:10:39
 *  Author: Polański Artur, Bajorek Aleksandra
 */


#define F_CPU 16000000UL
#include <avr/io.h>
#include "HD44780.h"

int main(void)
{
    DDRA = 0xFF;
    LCD_Initalize();
    LCD_Text("Bardzo dlugi smieszny napis");
    
   
    while(1)
    {
        LCD_Command(0b00011000);
        _delay_ms(1000);
    }
}



//plik HD44780.c


#define F_CPU 16000000UL
#include <avr/io.h>
#include "HD44780.h"
#define E 0b100000
#define RS 0b10000


void WriteNibble(unsigned char nibbleToWrite)
{
    PORTA |= E; // ustawiamy 1 na E
    PORTA = (PORTA & 0xF0) | (nibbleToWrite & 0x0F); // ustawiamy 4 mlodsze bity z nibbleToWrite na D4 D5 D6 D7
    PORTA &= ~E; // ustawiamy 0 na E
}


void WriteByte(unsigned char dataToWrite)
{
    WriteNibble(dataToWrite>>4);    //najpierw 4 starsze bity
    WriteNibble(dataToWrite);            //potem 4 młodsze
}


void LCD_Command(unsigned char x)
{
    PORTA &= ~RS; // ustawiamy 0 na RS (komenda)
    WriteByte(x);
};

void LCD_Text(char * x)
{
    PORTA |= RS;
    int i=0;
    while(x[i]!='\0')
    {
        WriteByte(x[i]);
        _delay_ms(5);
        i++;
    }
};
void LCD_GoToXY(unsigned char x, unsigned char y)
{
    LCD_Command(0b10000000 | (x+0x40*y));
    _delay_ms(5);
};

void LCD_Clear(void)
{
    LCD_Command(0b00000001);
    _delay_ms(5);
};

void LCD_Home(void)
{
    LCD_Command(0b00000010);
    _delay_ms(5);
};

void LCD_Initalize(void)
{
    _delay_ms(50);
    WriteNibble(0b00000011);
    _delay_ms(5);
    WriteNibble(0b00000011);
    _delay_us(200);
    WriteNibble(0b00000011);
    _delay_ms(5);
    WriteNibble(0b00000010);
    _delay_ms(5);
    LCD_Command(0b00101000);
    _delay_ms(5);
    LCD_Command(0b00001000);
    _delay_ms(5);
    LCD_Command(0b00000001);
    _delay_ms(5);
    LCD_Command(0b00000110);
    _delay_ms(5);
    LCD_Command(0b00001111);
    _delay_ms(5);
    LCD_Command(0b10000000);
    _delay_ms(5);
};




//plik HD44780.h 

#ifndef __HD44780__H__
#define __HD44780__H__

#include <avr/io.h>
#include <util/delay.h>


#define HD44780_CLEAR                    0x01
#define HD44780_HOME                    0x02
#define HD44780_ENTRY_MODE                0x04
#define HD44780_DISPLAY_ONOFF            0x08
#define HD44780_DISPLAY_CURSOR_SHIFT    0x10
#define HD44780_FUNCTION_SET            0x20
#define HD44780_CGRAM_SET                0x40
#define HD44780_DDRAM_SET                0x80


#define HD44780_EM_SHIFT_CURSOR        0
#define HD44780_EM_SHIFT_DISPLAY    1
#define HD44780_EM_DECREMENT        0
#define HD44780_EM_INCREMENT        2


#define HD44780_DISPLAY_OFF            0
#define HD44780_DISPLAY_ON            4
#define HD44780_CURSOR_OFF            0
#define HD44780_CURSOR_ON            2
#define HD44780_CURSOR_NOBLINK        0
#define HD44780_CURSOR_BLINK        1


#define HD44780_SHIFT_CURSOR        0
#define HD44780_SHIFT_DISPLAY        8
#define HD44780_SHIFT_LEFT            0
#define HD44780_SHIFT_RIGHT            4


#define HD44780_FONT5x7                0
#define HD44780_FONT5x10            4
#define HD44780_ONE_LINE            0
#define HD44780_TWO_LINE            8
#define HD44780_4_BIT                0
#define HD44780_8_BIT                16

void LCD_Command(unsigned char);
void LCD_Text(char *);
void LCD_GoToXY(unsigned char, unsigned char);
void LCD_Clear(void);
void LCD_Home(void);
void LCD_Initalize(void);


#endif